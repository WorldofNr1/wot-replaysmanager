﻿
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=P0LIR0ID_wot-replaysmanager&metric=alert_status)](https://sonarcloud.io/dashboard?id=P0LIR0ID_wot-replaysmanager)

**Replays Manager** This is a modification for the game "World Of Tanks" which allows you to convenient view replays, results, playback, and uploading replays to wotreplays site

Code license: CC BY-NC-SA 4.0

P0LIR0ID 2016-2020

### An example of what a manager looks like
![An example of what a manager looks like](https://static.poliroid.ru/replaysManager.jpg)

